package com.traclabs.amp.reschu;

import java.io.IOException;
import java.net.InetSocketAddress;

import reschu.game.controller.Reschu;

import com.sun.net.httpserver.HttpServer;

/**
 * File created by TRACLabs
 * User: oliverl3
 * Date: 6/7/13
 */
public class ReschuServer implements Runnable{

    private HttpServer server;

    //this contructor deprecated and pending deletion
	/*public ReschuServer(Outputter o) throws Exception{
        this();
        outputRunner=o;
    }*/

    public ReschuServer(Reschu reschu) {

        try {
            server = HttpServer.create(new InetSocketAddress(8111), 0);
        } catch (IOException e) {
            System.out.println("exception with creating server");
            System.exit(0);
        }

        //the server creates the root context, the root creates the next level contexts, and so on down
        server.createContext("/Reschu", new ReschuHttpHandler(reschu, server));

        server.setExecutor(null); // creates a default executor
        server.start();
    }



    //we want the server to be on it's own thread, but we're not running anything other than handlers, so the run method is empty
    @Override
    public void run() {
    }
    
}

