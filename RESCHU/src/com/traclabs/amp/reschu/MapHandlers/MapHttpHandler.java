package com.traclabs.amp.reschu.MapHandlers;

import com.google.gson.*;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import com.traclabs.amp.reschu.ReschuHttpHandler;
import com.traclabs.amp.reschu.TargetsObserver;
import reschu.game.model.Game;
import reschu.game.model.ReschuMap;

import java.io.IOException;
import java.io.OutputStream;

/**
 * File created by TRACLabs
 * User: oliverl3
 * Date: 6/10/13
 */
public class MapHttpHandler implements HttpHandler {
    
    Game game;
    
    public MapHttpHandler(Game g, HttpServer server) {
        game=g;

        server.createContext("/Reschu/map/maparray", new MapArrayHttpHandler(game));
        //server.createContext("/Reschu/map/assignedtargets", new AssignedTargetsHttpHandler(game, server));
        server.createContext("/Reschu/map/targets", new TargetsOnMapHttpHandler(game, server));
        server.createContext("/Reschu/map/hazards", new HazardsHttpHandler(game, server));
        //server.createContext("/Reschu/map/destroyedtargets", new DestroyedTargetsHttpHandler(game, server));
        
        new TargetsObserver(server, game);
        
    }

    @Override
    public void handle(HttpExchange exchange) throws IOException {

        if (ReschuHttpHandler.checkInvalidPath(exchange)) return;

        OutputStream os = exchange.getResponseBody();
        GsonBuilder gsonBuilder=new GsonBuilder();
        gsonBuilder.registerTypeAdapter(ReschuMap.class, new MapSerializer());
        Gson gson=gsonBuilder.create();
        String JsonResponse=gson.toJson(game.reschuMap);

        exchange.sendResponseHeaders(200, JsonResponse.length());
        os.write(JsonResponse.getBytes());

        os.close();
    }
}

