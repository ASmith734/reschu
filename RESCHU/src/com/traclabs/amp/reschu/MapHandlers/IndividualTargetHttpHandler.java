package com.traclabs.amp.reschu.MapHandlers;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.util.Map;

import reschu.game.model.Game;
import reschu.game.model.Target;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import com.traclabs.amp.reschu.PositionObject;
import com.traclabs.amp.reschu.ReschuHttpHandler;

/**
 * File created by TRACLabs User: oliverl3 Date: 6/10/13
 */
public class IndividualTargetHttpHandler implements HttpHandler {

	Game game;
	private String targName;
	Target Target;

	public IndividualTargetHttpHandler(Game g, HttpServer server, String name) {

		game = g;
		targName = name;
		Target = game.reschuMap.getMapDestroyedTargets().get(name);

		server.createContext("/Reschu/Targets/" + targName + "/name",
				new TargetNAMEHttpHandler(Target));
		server.createContext("/Reschu/Targets/" + targName + "/payload",
				new TargetPAYLOADHttpHandler(Target));
		server.createContext("/Reschu/Targets/" + targName + "/medium",
				new TargetMEDIUMHttpHandler(Target));
		server.createContext("/Reschu/Targets/" + targName + "/pos",
				new TargetPOSITIONHttpHandler(Target));
		server.createContext("/Reschu/Targets/" + targName + "/posX",
				new TargetPOSITIONXHttpHandler(Target));
		server.createContext("/Reschu/Targets/" + targName + "/posY",
				new TargetPOSITIONYHttpHandler(Target));
		server.createContext("/Reschu/Targets/" + targName + "/posZ",
				new TargetPOSITIONZHttpHandler(Target));
		server.createContext("/Reschu/Targets/" + targName + "/destroyed",
				new TargetDESTROYEDHttpHandler(Target));

	}

	@Override
	public void handle(HttpExchange exchange) throws IOException {
		// /Reschu/Targets/1

		if (exchange.getRequestMethod().equals("GET")) {

			OutputStream os = exchange.getResponseBody();
			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.registerTypeAdapter(Target.getClass(),
					new TargetSerializer());
			Gson gson = gsonBuilder.create();
			String JsonResponse = gson.toJson(Target);

			exchange.sendResponseHeaders(200, JsonResponse.length());
			os.write(JsonResponse.getBytes());

			os.close();
		} else if (exchange.getRequestMethod().equals("POST")) {

			Map<String, Object> parameters = ReschuHttpHandler
					.parsePostParameters(exchange);
			OutputStream os = exchange.getResponseBody();

			System.out.println("in Target's post section");
			if (parameters.containsKey("engage")) {
				// FIXME!!!!
			}

			else {
				Gson gson = new Gson();
				String commNotSup = gson
						.toJson("invalid command or no parameter received");
				exchange.sendResponseHeaders(400, commNotSup.length());
				os.write(commNotSup.getBytes());
			}

			os.close();
		} else {
			OutputStream os = exchange.getResponseBody();
			String methNotSup = "method not supported";
			exchange.sendResponseHeaders(400, methNotSup.length());
			os.write(methNotSup.getBytes());
			os.close();
		}

		/*
		 * if (ReschuHttpHandler.checkInvalidPath(exchange)) return;
		 * 
		 * //handle filter querries
		 * 
		 * 
		 * OutputStream os = exchange.getResponseBody(); GsonBuilder
		 * gsonBuilder=new GsonBuilder();
		 * gsonBuilder.registerTypeAdapter(Target.getClass(), new
		 * TargetSerializer()); Gson gson=gsonBuilder.create(); String
		 * JsonResponse=gson.toJson(Target);
		 * 
		 * exchange.sendResponseHeaders(200, JsonResponse.length());
		 * os.write(JsonResponse.getBytes());
		 * 
		 * os.close();
		 */
	}

}

class TargetSerializer implements JsonSerializer<Target> {

	@Override
	public JsonElement serialize(Target src, Type typeOfSrc,
			JsonSerializationContext context) {

		JsonObject TargetJson = new JsonObject();
		Gson gson = new Gson();

		TargetJson.addProperty("name", src.getName());
		TargetJson.add("availablePayloads",
				gson.toJsonTree(src.getAvailablePayloads()));
		TargetJson.addProperty("medium", gson.toJson(src.getMedium()));
		TargetJson.add("position",
				gson.toJsonTree(new PositionObject(src.getX(), src.getY())));
		TargetJson.addProperty("positionX", src.getX());
		TargetJson.addProperty("positionY", src.getY());
		TargetJson.addProperty("positionX",  src.getZ());
		TargetJson.addProperty("isDestroyed", src.isDestroyed());

		return TargetJson;
	}
}