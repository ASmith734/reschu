package com.traclabs.amp.reschu.MapHandlers;

import java.io.IOException;
import java.io.OutputStream;

import reschu.game.model.Target;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.traclabs.amp.reschu.ReschuHttpHandler;

/**
 * File created by TRACLabs
 * User: oliverl3
 * Date: 6/11/13
 */
public class TargetMEDIUMHttpHandler implements HttpHandler {

    private Target target;

    public TargetMEDIUMHttpHandler(Target v) {
        target = v;
    }

    @Override
    public void handle(HttpExchange exchange) throws IOException {

        if (ReschuHttpHandler.checkInvalidPath(exchange)) return;

        OutputStream os = exchange.getResponseBody();

        Gson gson=new Gson();
        String JsonResponse=gson.toJson(target.getMedium());

        exchange.sendResponseHeaders(200, JsonResponse.length());
        os.write(JsonResponse.getBytes());

        os.close();
        
    }
}
