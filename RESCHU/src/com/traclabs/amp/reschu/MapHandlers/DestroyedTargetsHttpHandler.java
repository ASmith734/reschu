package com.traclabs.amp.reschu.MapHandlers;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.LinkedList;

import reschu.game.model.DestroyedTarget;
import reschu.game.model.Game;
import reschu.game.model.Target;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import com.traclabs.amp.reschu.DestroyedTargetSerializer;

public class DestroyedTargetsHttpHandler implements HttpHandler {

	
	private HttpServer server;

	private ArrayList<DestroyedTarget> destroyedTargetList;

	public DestroyedTargetsHttpHandler(Game game, HttpServer s) {

		server = s;

		destroyedTargetList = new ArrayList<DestroyedTarget>(game.reschuMap
				.getMapDestroyedTargets().values());

		for (DestroyedTarget targ : destroyedTargetList) {

			server.createContext(
					"/Reschu/map/destroyedTargets/" + targ.getName(),
					new IndividualTargetHttpHandler(game, server, targ
							.getName()));

		}
	}

	@Override
	public void handle(HttpExchange exchange) throws IOException {

		String requestStr = exchange.getRequestURI().getPath();
		String[] requestPath = requestStr.split("/");

		String contextStr = exchange.getHttpContext().getPath();
		String[] contextPath = contextStr.split("/");

		OutputStream os = exchange.getResponseBody();

		if (requestPath.length == contextPath.length) {

			GsonBuilder gsonBuilder = new GsonBuilder();

			gsonBuilder.registerTypeAdapter(ArrayList.class,
					new TargetsInListSerializer("listDestroyedTargets"));

			Gson gson = gsonBuilder.create();

			String JsonResponse = gson.toJson(destroyedTargetList);

			exchange.sendResponseHeaders(200, JsonResponse.length());
			os.write(JsonResponse.getBytes());

		} else if (requestPath[4].equals("numberofassignedtargets")) {

			Gson gson = new Gson();

			String JsonResponse = gson.toJson(destroyedTargetList.size());
			exchange.sendResponseHeaders(200, JsonResponse.length());
			os.write(JsonResponse.getBytes());

		} else {

			String notRecogResponse = "<h1>requestPath not recognized</h1>";
			exchange.sendResponseHeaders(400, notRecogResponse.length());
			os.write(notRecogResponse.getBytes());
		}

		os.close();

	}

	// public void handle(HttpExchange exchange) throws IOException {
	//
	// // /Reschu/map/assignedtargets
	//
	// LinkedList<String> targetNames = new LinkedList<String>();
	//
	// for (int i = 0; i < destroyedTargetList.size(); i++) {
	//
	// targetNames.addAll(destroyedTargetList.keySet());
	//
	// }
	//
	// String requestStr = exchange.getRequestURI().getPath();
	// String[] requestPath = requestStr.split("/");
	//
	// String contextStr = exchange.getHttpContext().getPath();
	// String[] contextPath = contextStr.split("/");
	//
	// OutputStream os = exchange.getResponseBody();
	//
	// if (requestPath.length == contextPath.length) {
	//
	// GsonBuilder gsonBuilder = new GsonBuilder();
	//
	// gsonBuilder
	// .registerTypeAdapter(LinkedList.class,
	// new DestroyedTargetsInListSerializer(
	// "listDestroyedTargets"));
	//
	// Gson gson = gsonBuilder.create();
	//
	// String JsonResponse = gson.toJson(destroyedTargetList);
	//
	// exchange.sendResponseHeaders(200, JsonResponse.length());
	// os.write(JsonResponse.getBytes());
	//
	// } else if (requestPath[4].equals("numberofdestroyedtargets")) {
	//
	// Gson gson = new Gson();
	//
	// String JsonResponse = gson.toJson(destroyedTargetList.size());
	// exchange.sendResponseHeaders(200, JsonResponse.length());
	// os.write(JsonResponse.getBytes());
	//
	// } else {
	//
	// String notRecogResponse = "<h1>requestPath not recognized</h1>";
	// exchange.sendResponseHeaders(400, notRecogResponse.length());
	// os.write(notRecogResponse.getBytes());
	// }
	//
	// os.close();
	//
	// }
	// }

	class DestroyedTargetsInListSerializer implements
			JsonSerializer<LinkedList<DestroyedTarget>> {

		@Override
		public JsonElement serialize(LinkedList<DestroyedTarget> src,
				Type typeOfSrc, JsonSerializationContext context) {

			JsonObject listJson = new JsonObject();

			Gson gson = new Gson();

			LinkedList<JsonElement> list = new LinkedList<JsonElement>();

			GsonBuilder targetGsonBuilder = new GsonBuilder();
			targetGsonBuilder.registerTypeAdapter(Target.class,
					new DestroyedTargetSerializer());
			Gson targetGson = targetGsonBuilder.create();

			for (int i = 0; i < src.size(); i++) {

				list.add(i, targetGson.toJsonTree(src.get(i)));

			}

			listJson.add("TargetList", gson.toJsonTree(list));

			return listJson;
		}

		public DestroyedTargetsInListSerializer() {

		}

	}
}
