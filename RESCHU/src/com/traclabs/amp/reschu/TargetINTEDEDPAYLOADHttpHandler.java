package com.traclabs.amp.reschu;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import reschu.game.model.Target;

import java.io.IOException;
import java.io.OutputStream;

/**
 * File created by TRACLabs
 * User: oliverl3
 * Date: 7/1/13
 */
public class TargetINTEDEDPAYLOADHttpHandler implements HttpHandler {
    
    private Target target;
    
    public TargetINTEDEDPAYLOADHttpHandler(Target t) {
        
        target=t;
    }

    @Override
    public void handle(HttpExchange exchange) throws IOException {
        if (ReschuHttpHandler.checkInvalidPath(exchange)) return;

        OutputStream os = exchange.getResponseBody();

        Gson gson=new Gson();

        String JsonResponse=gson.toJson(target.getAvailablePayloads());

        exchange.sendResponseHeaders(200, JsonResponse.length());
        os.write(JsonResponse.getBytes());

        os.close();
    }
}
