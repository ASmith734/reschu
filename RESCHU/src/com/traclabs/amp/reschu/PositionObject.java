package com.traclabs.amp.reschu;

/**
 * File created by TRACLabs
 * User: oliverl3
 * Date: 6/11/13
 */
public class PositionObject {


    int x;
    int y;
    int z;

    public PositionObject(int x, int y){


        this.x = x;
        this.y = y;
        this.z = 0;
    }
    
    public PositionObject(int x, int y, int z){


        this.x = x;
        this.y = y;
        this.z = z;
    }
    
    public PositionObject(int [] pos){


        x=pos[0];
        y=pos[1];
        z = (pos.length > 2) ? pos[2] : 0;
    }

}