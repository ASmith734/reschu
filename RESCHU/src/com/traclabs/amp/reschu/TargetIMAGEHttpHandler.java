package com.traclabs.amp.reschu;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;

import javax.imageio.stream.ImageOutputStream;
import javax.imageio.stream.MemoryCacheImageOutputStream;

import reschu.game.model.Target;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;


/**
 * File created by TRACLabs User: oliverl3 Date: 6/11/13
 */
public class TargetIMAGEHttpHandler implements HttpHandler {

	Target target;

	public TargetIMAGEHttpHandler(Target t) {
		target = t;
	}

	@Override
	public void handle(HttpExchange exchange) throws IOException {

		if (ReschuHttpHandler.checkInvalidPath(exchange))
			return;

		
		try {
			
		      // add the required response header for a PDF file
		      Headers h = exchange.getResponseHeaders();
		      h.add("Content-Type", "application/html");
		      
		      String imageFilePath = Target.class.getClassLoader().getResource(target.getImageFileName()).getPath();
		      
		      File file = new File (imageFilePath);
		      byte [] byteArray  = new byte [(int)file.length()];
		      FileInputStream fis = new FileInputStream(file);
		      BufferedInputStream bis = new BufferedInputStream(fis);
		      bis.read(byteArray, 0, byteArray.length);

		      // ok, we are ready to send the response.
		      exchange.sendResponseHeaders(200, file.length());
		     
		      OutputStream os = exchange.getResponseBody();
		     
		      
		      os.write(byteArray,0,byteArray.length);
		      os.close();
//BufferedImage targetImage;
//			targetImage = target.getCurrentImage();
//
//			OutputStream os = exchange.getResponseBody();
//			exchange.getResponseHeaders().set("Content-type", "image/jpg");
//
//			ImageOutputStream ios = ImageIO.createImageOutputStream(os);
//
//			ImageWriter writer = null;
//			Iterator<ImageWriter> iter = ImageIO
//					.getImageWritersByFormatName("jpg");
//			if (iter.hasNext()) {
//				writer = (ImageWriter) iter.next();
//			}
//
//			writer.setOutput(ios);
//
//			exchange.sendResponseHeaders(200, target.getImageSize());
//			
//			writer.write(new IIOImage(targetImage, null, null));
//
//			ios.flush();
//
//			writer.dispose();
//			ios.close();
//			os.close();

		} catch (IOException e) {
			System.out.println("Target " + target.getName()
					+ "'s image " + target.getImageFileName() + " could not load. Exception: " + e.getLocalizedMessage());

		}

	}
}
