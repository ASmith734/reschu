package com.traclabs.amp.reschu.VehicleHandlers;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.traclabs.amp.reschu.ReschuHttpHandler;
import reschu.game.model.Vehicle;

import java.io.IOException;
import java.io.OutputStream;

/**
 * File created by TRACLabs User: oliverl3 Date: 6/13/13
 */
public class VehicleLASTCOMMANDSTATUSHttpHandler implements HttpHandler {

	private Vehicle vehicle;

	public VehicleLASTCOMMANDSTATUSHttpHandler(Vehicle v) {
		vehicle = v;
	}

	@Override
	public void handle(HttpExchange exchange) throws IOException {

		// the following in reschu.constants.MyGame.java
		/*
		 * final static public int STATUS_VEHICLE_LAST_COMMAND_SUCCESSFUL = 0;
		 * final static public int STATUS_VEHICLE_LAST_COMMAND_PENDING = 1;
		 * final static public int STATUS_VEHICLE_LAST_COMMAND_FAILED = 2;
		 */

		if (ReschuHttpHandler.checkInvalidPath(exchange))
			return;

		OutputStream os = exchange.getResponseBody();

		Gson gson = new Gson();

		String JsonResponse = gson.toJson(vehicle.getLastCommandStatus());

		exchange.sendResponseHeaders(200, JsonResponse.length());
		os.write(JsonResponse.getBytes());

		os.close();

	}
}
