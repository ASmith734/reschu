package com.traclabs.amp.reschu.VehicleHandlers;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.util.EnumSet;
import java.util.LinkedList;
import java.util.Map;

import reschu.constants.MyGame;
import reschu.game.controller.Reschu;
import reschu.game.model.Game;
import reschu.game.model.Target;
import reschu.game.model.Vehicle;
import reschu.game.model.Vehicle.Payload;
import reschu.game.utils.Utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import com.traclabs.amp.reschu.PositionObject;
import com.traclabs.amp.reschu.ReschuHttpHandler;
import com.traclabs.amp.reschu.TargetSerializer;

/**
 * File created by TRACLabs User: oliverl3 Date: 6/10/13
 */
public class IndividualVehicleHttpHandler implements HttpHandler {

	Reschu reschu;
	Game game;
	private int index;
	Vehicle vehicle;

	public IndividualVehicleHttpHandler(Reschu r, HttpServer server, int i) {
		reschu = r;
		game = reschu.game;
		index = i;
		vehicle = game.getVehicleList().getVehicle(index);

		server.createContext("/Reschu/vehicles/" + index + "/name",
				new VehicleNAMEHttpHandler(vehicle));
		server.createContext("/Reschu/vehicles/" + index + "/class",
				new VehicleCLASSHttpHandler(vehicle));
		server.createContext("/Reschu/vehicles/" + index + "/payload",
				new VehiclePAYLOADHttpHandler(vehicle));
		server.createContext("/Reschu/vehicles/" + index + "/medium",
				new VehicleMEDIUMHttpHandler(vehicle));
		server.createContext("/Reschu/vehicles/" + index + "/pos",
				new VehiclePOSITIONHttpHandler(vehicle));
		server.createContext("/Reschu/vehicles/" + index + "/velocity",
				new VehicleVELOCITYHttpHandler(vehicle));
		// the target handler is now created in TargetsObserver. achieves the
		// same thing as the below commented out line
		// server.createContext("/Reschu/vehicles/"+index+"/target", new
		// TargetHttpHandler(game, vehicle.getTarget(),server,
		// "/Reschu/vehicles/" + index + "/target"));
		server.createContext("/Reschu/vehicles/" + index + "/assignedTarget",
				new VehicleASSIGNEDTARGETHttpHandler(vehicle));
		server.createContext("/Reschu/vehicles/" + index + "/path",
				new VehiclePATHHttpHandler(game, vehicle, server));

		server.createContext("/Reschu/vehicles/" + index + "/status",
				new VehicleSTATUSHttpHandler(vehicle));
		server.createContext("/Reschu/vehicles/" + index + "/damage",
				new VehicleDAMAGEHttpHandler(vehicle));
		server.createContext("/Reschu/vehicles/" + index + "/uuvstuck",
				new VehicleUUVSTUCKHttpHandler(vehicle));
		server.createContext("/Reschu/vehicles/" + index + "/intersect",
				new VehicleINTERSECTHttpHandler(vehicle));

		server.createContext("/Reschu/vehicles/" + index + "/ttt",
				new VehicleTIMETOTARGETHttpHandler(vehicle));

		server.createContext("/Reschu/vehicles/" + index + "/grounded",
				new VehicleGROUNDEDHttpHandler(vehicle));
		server.createContext("/Reschu/vehicles/" + index + "/positionX",
				new VehiclePOSITIONXHttpHandler(vehicle));
		server.createContext("/Reschu/vehicles/" + index + "/positionY",
				new VehiclePOSITIONYHttpHandler(vehicle));
		server.createContext(
				"/Reschu/vehicles/" + index + "/lastCommandStatus",
				new VehicleLASTCOMMANDSTATUSHttpHandler(vehicle));
	}

	public int getIndex() {
		return index;
	}

	@Override
	public void handle(HttpExchange exchange) throws IOException {
		// /Reschu/vehicles/1

		if (exchange.getRequestMethod().equals("GET")) {

			OutputStream os = exchange.getResponseBody();
			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.registerTypeAdapter(vehicle.getClass(),
					new VehicleSerializer());
			Gson gson = gsonBuilder.create();
			String JsonResponse = gson.toJson(vehicle);

			exchange.sendResponseHeaders(200, JsonResponse.length());
			os.write(JsonResponse.getBytes());

			os.close();
		} else if (exchange.getRequestMethod().equals("POST")) {

			Map<String, Object> parameters = ReschuHttpHandler
					.parsePostParameters(exchange);
			OutputStream os = exchange.getResponseBody();

			System.out.println("in vehicle's post section");

			if (parameters.containsKey("engage")) {

				if (vehicle.getStatus() != 2) {

					Gson gson = new Gson();
					String commInvalid = gson
							.toJson("command invalid, must be pending to engage a target in order to engage");
					exchange.sendResponseHeaders(400, commInvalid.length());
					os.write(commInvalid.getBytes());
					os.close();
					return;
				}

				EnumSet<Vehicle.Payload> targetPayloads = vehicle.getTarget()
						.getAvailablePayloads();

				if (targetPayloads.contains(Vehicle.Payload.AIRBASE)
						|| targetPayloads.contains(Vehicle.Payload.SEABASE)) {

					Gson gson = new Gson();
					String cannotEngageThisResponse = gson
							.toJson("you don't engage your own base, you land on it you idiot");
					exchange.sendResponseHeaders(400,
							cannotEngageThisResponse.length());
					os.write(cannotEngageThisResponse.getBytes());
					os.close();
					return;
				}

				for (int i = 0; i < game.getVehicleList().size(); i++) {
					if (game.getVehicleList().getVehicle(i).getStatus() == MyGame.STATUS_VEHICLE_PAYLOAD) {

						Gson gson = new Gson();
						String commNotSup = gson
								.toJson("Cannot engage when another vehicle is already engaging, operator can only engage one thing at a time");
						exchange.sendResponseHeaders(400, commNotSup.length());
						os.write(commNotSup.getBytes());

						return;
					}
				}

				boolean success = reschu.Engage(vehicle);

				if (success) {
					Gson gson = new Gson();
					String response = gson.toJson("ENGAGING");
					exchange.sendResponseHeaders(200, response.length());
					os.write(response.getBytes());
				}else{
					Gson gson = new Gson();
					String response = gson.toJson("Could Not Engage!!");
					exchange.sendResponseHeaders(400, response.length());
					os.write(response.getBytes());
				}
			} else if (parameters.containsKey("takeoff")) {

				if (!vehicle.isGrounded()) {

					Gson gson = new Gson();
					String cannotTakeoffResponse = gson
							.toJson("you cannot takeoff or launch, you are already in the air or water");
					exchange.sendResponseHeaders(400,
							cannotTakeoffResponse.length());
					os.write(cannotTakeoffResponse.getBytes());
					os.close();
					return;
				}
				System.out.println("in takeoff sequence");

				vehicle.unground();

				Gson gson = new Gson();
				String response;
				if (vehicle.getMedium() == Vehicle.TerrainMedium.AIR) {
					response = gson.toJson("TAKING OFF");
				} else {
					response = gson.toJson("LAUNCHING");
				}
				exchange.sendResponseHeaders(200, response.length());
				os.write(response.getBytes());

			} else if (parameters.containsKey("land")) {

				System.out.println("in landing sequence");

				if (vehicle.isGrounded()) {

					Gson gson = new Gson();
					String commInvalid = gson
							.toJson("command invalid, must not be grounded in order to land");
					exchange.sendResponseHeaders(400, commInvalid.length());
					os.write(commInvalid.getBytes());
					os.close();
					return;
				}
				// Check to see if vehicle has been assigned a target

				if (vehicle.getTarget() != null) {
					Target tempTarget = vehicle.getTarget();

					double howFarFromTarget = Utils.distanceBetween(
							vehicle.getX(), vehicle.getY(), tempTarget.getX(),
							tempTarget.getY());

					// Check if we are close enough to Assigned Target to land:
					if (howFarFromTarget > 10.0) {
						Gson gson = new Gson();
						String cannotLandOnThisResponse = gson
								.toJson("Vehicle is too far from Target to Land.");
						exchange.sendResponseHeaders(400,
								cannotLandOnThisResponse.length());
						os.write(cannotLandOnThisResponse.getBytes());
						os.close();
						return;
					}

					if (!((tempTarget.getAvailablePayloads()
							.contains(Vehicle.Payload.AIRBASE)) || (tempTarget
							.getAvailablePayloads()
							.contains(Vehicle.Payload.SEABASE)))) {

						Gson gson = new Gson();
						String cannotLandOnThisResponse = gson
								.toJson("you can't land on this");
						exchange.sendResponseHeaders(400,
								cannotLandOnThisResponse.length());
						os.write(cannotLandOnThisResponse.getBytes());
						os.close();
						return;
					} else {
						// vehicle.setActivePayload(Payload.AIRBASE);

						vehicle.ground();

						Gson gson = new Gson();
						String response = gson.toJson("LANDING");
						exchange.sendResponseHeaders(200, response.length());
						os.write(response.getBytes());
					}
				} else {
					// Vehicle has no target, and needs one to land

					System.out
							.println("Attempted to land vehicle with no assigned landing site.");
					vehicle.ground();

					Gson gson = new Gson();
					String response = gson.toJson("LANDING");
					exchange.sendResponseHeaders(200, response.length());
					os.write(response.getBytes());
					
					
//					Gson gson = new Gson();
//					String noTargetAssigned = gson
//							.toJson("Attempted to land vehicle with no assigned landing site.");
//					exchange.sendResponseHeaders(400, noTargetAssigned.length());
//					os.write(noTargetAssigned.getBytes());
				}

			} else {
				Gson gson = new Gson();
				String commNotSup = gson
						.toJson("invalid command or no parameter received");
				exchange.sendResponseHeaders(400, commNotSup.length());
				os.write(commNotSup.getBytes());
			}

			os.close();
		} else {
			OutputStream os = exchange.getResponseBody();
			String methNotSup = "method not supported";
			exchange.sendResponseHeaders(400, methNotSup.length());
			os.write(methNotSup.getBytes());
			os.close();
		}

		/*
		 * if (ReschuHttpHandler.checkInvalidPath(exchange)) return;
		 * 
		 * //handle filter querries
		 * 
		 * 
		 * OutputStream os = exchange.getResponseBody(); GsonBuilder
		 * gsonBuilder=new GsonBuilder();
		 * gsonBuilder.registerTypeAdapter(vehicle.getClass(), new
		 * VehicleSerializer()); Gson gson=gsonBuilder.create(); String
		 * JsonResponse=gson.toJson(vehicle);
		 * 
		 * exchange.sendResponseHeaders(200, JsonResponse.length());
		 * os.write(JsonResponse.getBytes());
		 * 
		 * os.close();
		 */
	}
}

class VehicleSerializer implements JsonSerializer<Vehicle> {

	@Override
	public JsonElement serialize(Vehicle src, Type typeOfSrc,
			JsonSerializationContext context) {

		JsonObject vehicleJson = new JsonObject();
		Gson gson = new Gson();

		vehicleJson.addProperty("name", src.getName());
		vehicleJson.addProperty("class", gson.toJson(src.getVehicleClass()));
		vehicleJson.add("activePayload",
				gson.toJsonTree(src.getActivePayload()));
		vehicleJson.add("availablePayloads",
				gson.toJsonTree(src.getAvailablePayloads()));
		vehicleJson.addProperty("medium", gson.toJson(src.getMedium()));
		vehicleJson.add("position",
				gson.toJsonTree(new PositionObject(src.getX(), src.getY())));
		vehicleJson.addProperty("positionX", src.getX());
		vehicleJson.addProperty("positionY", src.getY());
		vehicleJson.add("velocity", gson.toJsonTree(new VelocityObject(src
				.getVelocityNominal(), src.getVelocityActual())));
		vehicleJson.addProperty("status", src.getStatus());
		vehicleJson.addProperty("lastCommandStatus", src.getLastCommandStatus());
		vehicleJson.addProperty("damage", src.getDamage());
		vehicleJson.addProperty("intersect", src.getIntersect());
		vehicleJson.addProperty("ttt", src.calcTimeToTarget());
		if (src.getTarget() != null) {
			vehicleJson
					.addProperty("assignedTarget", src.getTarget().getName());
		} else {
			vehicleJson.addProperty("assignedTarget", "NONE");
		}

		GsonBuilder gsonTargetBuilder = new GsonBuilder();
		gsonTargetBuilder.registerTypeAdapter(Target.class,
				new TargetSerializer());
		Gson targetgson = gsonTargetBuilder.create();
		vehicleJson.add("target", targetgson.toJsonTree(src.getTarget()));

		GsonBuilder gsonPathBuilder = new GsonBuilder();
		gsonPathBuilder.registerTypeAdapter(LinkedList.class,
				new PathSerializer());
		Gson pathgson = gsonPathBuilder.create();
		vehicleJson.add("path", pathgson.toJsonTree(src.getPath()));

		vehicleJson.add("uuvstuck", gson.toJsonTree(new UUVStuckInfo(src
				.isUUV_stuck(), src.getUUV_stuck_count())));

		vehicleJson.addProperty("grounded", src.isGrounded());

		return vehicleJson;
	}
}