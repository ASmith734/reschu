package com.traclabs.amp.reschu.VehicleHandlers;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.traclabs.amp.reschu.ReschuHttpHandler;
import reschu.game.model.Vehicle;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

/**
 * File created by TRACLabs
 * User: oliverl3
 * Date: 6/11/13
 */
public class VehicleVELOCITYHttpHandler implements HttpHandler {
    private Vehicle vehicle;

    public VehicleVELOCITYHttpHandler(Vehicle v) {
        vehicle = v;
    }

    @Override
    public void handle(HttpExchange exchange) throws IOException {


        if(exchange.getRequestMethod().equals("GET")){

            OutputStream os = exchange.getResponseBody();

            Gson gson=new Gson();

            String JsonResponse=gson.toJson(new VelocityObject(vehicle.getVelocityNominal(), vehicle.getVelocityActual()));

            exchange.sendResponseHeaders(200, JsonResponse.length());
            os.write(JsonResponse.getBytes());

            os.close();
        }
        else if(exchange.getRequestMethod().equals("POST")){
            Map<String, Object> parameters = ReschuHttpHandler.parsePostParameters(exchange);
            OutputStream os = exchange.getResponseBody();

            double vel;
            try {vel = Double.parseDouble((String) parameters.get("velocity"));}
            
            catch (NumberFormatException e) {
                String errorResponse = "velocity misformated";
                exchange.sendResponseHeaders(400, errorResponse.length());
                os.write(errorResponse.getBytes());
                return;
            }
            catch(NullPointerException e){
            	String errorResponse = "velocity not given";
                exchange.sendResponseHeaders(400, errorResponse.length());
                os.write(errorResponse.getBytes());
                return;
            }
            
//            if(vel){
//            	   String errorResponse = "velocity misformated or not given";
//                   exchange.sendResponseHeaders(400, errorResponse.length());
//                   os.write(errorResponse.getBytes());
//                   return;
//            }
            
            vehicle.setVelocityNominal(vel);

            Gson gson=new Gson();

            String JsonResponse = gson.toJson("velocity successfully set to "+vel);

            exchange.sendResponseHeaders(200, JsonResponse.length());
            os.write(JsonResponse.getBytes());
            os.close();

        }
        else{
            OutputStream os = exchange.getResponseBody();
            String methNotSup="method not supported";
            exchange.sendResponseHeaders(400, methNotSup.length());
            os.write(methNotSup.getBytes());
            os.close();
        }

        /*if (ReschuHttpHandler.checkInvalidPath(exchange)) return;

        OutputStream os = exchange.getResponseBody();

        Gson gson=new Gson();

        String JsonResponse=gson.toJson(new Double(vehicle.getVelocityActual()));

        exchange.sendResponseHeaders(200, JsonResponse.length());
        os.write(JsonResponse.getBytes());

        os.close();*/
        
    }
}


class VelocityObject{
    
    double velocityNominal;
    double velocityActual;
    
    public VelocityObject(double nominal, double actual){
        velocityNominal=nominal;
        velocityActual=actual;
        
    }
    
    
    
}