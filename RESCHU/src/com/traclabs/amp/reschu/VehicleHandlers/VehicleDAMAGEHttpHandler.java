package com.traclabs.amp.reschu.VehicleHandlers;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.traclabs.amp.reschu.ReschuHttpHandler;
import reschu.game.model.Vehicle;

import java.io.IOException;
import java.io.OutputStream;

/**
 * File created by TRACLabs
 * User: oliverl3
 * Date: 6/11/13
 */
public class VehicleDAMAGEHttpHandler implements HttpHandler {
    
    Vehicle vehicle;
    
    public VehicleDAMAGEHttpHandler(Vehicle v) {
        vehicle=v;
    }

    @Override
    public void handle(HttpExchange exchange) throws IOException {

        if (ReschuHttpHandler.checkInvalidPath(exchange)) return;
        
        OutputStream os = exchange.getResponseBody();

        Gson gson=new Gson();


        String JsonResponse=gson.toJson(vehicle.getDamage());

        exchange.sendResponseHeaders(200, JsonResponse.length());
        os.write(JsonResponse.getBytes());

        os.close();
        
    }
}
