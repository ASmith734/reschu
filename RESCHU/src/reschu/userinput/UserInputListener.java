package reschu.userinput;

import java.util.EventListener;

public interface UserInputListener extends EventListener {
	// event dispatch methods
	public void UserInput(UserInputEvent e);
}
