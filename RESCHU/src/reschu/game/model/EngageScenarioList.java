package reschu.game.model;

import java.util.LinkedList;
import java.util.Random;

import reschu.game.controller.Reschu;

public class EngageScenarioList {
	private LinkedList<EngageScenario> engagescenario_list = new LinkedList<EngageScenario>();
	private Random rnd;

	public EngageScenarioList() {
		rnd = new Random(100);
	}

	public int size() {
		return engagescenario_list.size();
	}

	public void addEngageScenario(int idx, String vType, String tType, int[] loc,
                                  String stmt) {
		engagescenario_list.addLast(new EngageScenario(idx, loc, vType, tType, stmt));
	}

	public EngageScenario getEngageScenario(Vehicle.TerrainMedium vType, Vehicle.TerrainMedium tType) {
		EngageScenario p = null;
		int cnt = 0;

		if (Reschu.train() || Reschu.tutorial()) {
			for (int i = 0; i < engagescenario_list.size(); i++) {
				p = engagescenario_list.get(i);
				if (p.getVehicleType().equals(vType)
						&& p.getTargetType().equals(tType))
					break;
			}
		}

		else {
			do {
				p = engagescenario_list.get(rnd.nextInt(engagescenario_list.size() - 1));
				if (++cnt >= engagescenario_list.size())
					return null;
			} while (!p.getVehicleType().equals(vType) || !p.getTargetType().equals(tType) || p.isDone());
			p.setDone(true);
		}
		return p;
	}
}
