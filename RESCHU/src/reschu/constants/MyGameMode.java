package reschu.constants;

public class MyGameMode {
	final static public int ADMINISTRATOR_MODE = 0;
	final static public int USER_MODE = 1;
	final static public int TUTORIAL_MODE = 2;
	final static public int TRAIN_MODE = 3;
	final static public int REPLAY_MODE = 4;
}
