@echo off
set "RESCHU_HOME=%~dp0%"
set "mainClass=reschu.app.AppMain"
set "binDir=%RESCHU_HOME%bin"
set "resourceDir=%RESCHU_HOME%resources"
set "libDir=%RESCHU_HOME%lib"
set "libClasspath=%libDir%\jogl.jar;%libDir%\comm.jar;%libDir%\corecomponents-swing-win.jar;%libDir%\gluegen-rt.jar;%libDir%\Jama-1.0.2.jar;%libDir%\mysql-connector-java-5.1.5-bin.jar;%libDir%\TableLayout-bin-jdk1.5-2007-04-21.jar;%libDir%\TimingFramework-0.53.jar;%libDir%\webrenderer-swing-win.jar;%libDir%\gson-2.2.4.jar;%libDir%\webrenderer-swing.jar"
set "jcp=%binDir%;%resourceDir%;%libClasspath%"
set "libPath=-Djava.library.path=%libDir%\jogl-natives-windows-amd64;%libDir%\jogl-natives-windows-i586;%libDir%\gluegen-rt-natives-windows-amd64;%libDir%\gluegen-rt-natives-windows-i586"

rem @echo "Run: java -Xmx1g -Xms500m -cp %jcp% %libPath% %mainClass% %*"
java -Xmx1g -Xms500m -cp %jcp% %libPath% %mainClass% %*
